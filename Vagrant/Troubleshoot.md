**Got different reports about installed GuestAdditions version during Vagrant Up**

![Screenshot: Error message from Got different reports about installed GuestAdditions version](/Images/vagrant-different-installed-guestadditions-version.png "Screenshot: Error message from Got different reports about installed GuestAdditions version")

```
vagrant plugin update
```
